import React, {useEffect, useState} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import NewsItem from './NewsItem';
import axios from 'axios';

type NewsItem = {
  id: number;
  image: string;
  title: string;
  date: string;
  description: string;
};

const News: React.FC = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [newsData, setNewsData] = useState<NewsItem[]>([]);

  const fetchNews = async () => {
    try {
      const response = await axios.get(
        'https://gitlab.com/oggmancuc/react-native-2024/-/raw/data/news.json',
      );
      const json = response.data;
      setNewsData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchNews();
  }, []);

  return (
    <View>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={newsData}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => <NewsItem news={item} />}
        />
      )}
    </View>
  );
};

export default News;
