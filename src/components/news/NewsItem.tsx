import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

interface NewsItemProps {
  news: {
    id: number;
    image: string;
    title: string;
    date: string;
    description: string;
  };
}

const NewsItem: React.FC<NewsItemProps> = ({news}) => {
  return (
    <View style={styles.container}>
      <Image source={{uri: news.image}} style={styles.image} />
      <View style={styles.content}>
        <Text style={styles.title}>{news.title}</Text>
        <Text style={styles.date}>{news.date}</Text>
        <Text style={styles.description}>{news.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e0e0e0',
    borderRadius: 8,
    padding: 16,
    flexDirection: 'row',
    marginBottom: 16,
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 8,
    marginRight: 16,
  },
  content: {
    flex: 1,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  date: {
    fontSize: 12,
    color: '#888',
    marginBottom: 8,
  },
  description: {
    fontSize: 14,
  },
});

export default NewsItem;
