import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import News from './news/News';
import Gallery from './gallery/Gallery';
import Profile from './profile/Profile';
import Icon from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();

const TabBarIcon =
  (name: string) =>
  ({color, size}: {color: string; size: number}) =>
    <Icon name={name} color={color} size={size} />;

const Menu: React.FC = () => {
  return (
    <Tab.Navigator initialRouteName="News">
      <Tab.Screen
        name="News"
        component={News}
        options={{
          tabBarIcon: props => TabBarIcon('newspaper-o')(props),
        }}
      />
      <Tab.Screen
        name="Gallery"
        component={Gallery}
        options={{
          tabBarIcon: props => TabBarIcon('image')(props),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: props => TabBarIcon('user')(props),
        }}
      />
    </Tab.Navigator>
  );
};

export default Menu;
