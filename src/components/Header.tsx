import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import {useColorScheme} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const Header = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    textColor: isDarkMode ? Colors.white : Colors.black,
  };

  return (
    <View
      style={styles.container}
      backgroundColor={backgroundStyle.backgroundColor}>
      <Image
        source={require('../assets/logos/logo.png')}
        style={styles.image}
      />
      <Text style={styles.text} color={backgroundStyle.textColor}>
        Новічков Єгор
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    height: 60,
  },
  image: {
    width: 48,
    height: 48,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
  },
});

export default Header;
