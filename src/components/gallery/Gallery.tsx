import React, {useEffect, useState} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import GalleryItem from './GalleryItem';
import axios from 'axios';

type GalleryItem = {
  id: number;
  url: string;
};

const Gallery: React.FC = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [galleryData, setGalleryData] = useState<GalleryItem[]>([]);

  const fetchNews = async () => {
    try {
      const response = await axios.get(
        'https://gitlab.com/oggmancuc/react-native-2024/-/raw/data/gallery.json',
      );
      const json = response.data;
      setGalleryData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchNews();
  }, []);

  return (
    <View>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={galleryData}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => <GalleryItem item={item} />}
        />
      )}
    </View>
  );
};

export default Gallery;
