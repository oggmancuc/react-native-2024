import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

type GalleryItemProps = {
  item: {
    id: number;
    url: string;
  };
};

const GalleryItem: React.FC<GalleryItemProps> = ({item}) => {
  return (
    <View style={styles.container}>
      <Image source={{uri: item.url}} style={styles.image} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '95%',
    margin: 10,
    aspectRatio: 16 / 9, // Adjust the aspect ratio as per your requirement
    backgroundColor: '#e0e0e0',
    borderRadius: 8,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
  },
});

export default GalleryItem;
